pub mod wmctrlrs;
pub mod tiling;

use std::env;

fn main() {
    env::set_var("RUST_BACKTRACE", "0");
    
    let window_info = wmctrlrs::get_window_info_batch("0x06c00003")
        .decimal_id()
        .desktop_id()
        .window_manager_class()
        .client_name()
        .title()
        .geometry()
        .decorations()
        .collect();

    println!("{:?}", window_info);

    // // Creating a layout
    // let mut layout = tiling::layout::master_stack(1);

    // // Modifying layout options
    // layout.set_layout_option(tiling::layout_option::master_percentage(0.7));
    // layout.set_layout_option(tiling::layout_option::outer_gaps(10, 10, 10, 10));
    // layout.set_layout_option(tiling::layout_option::inner_gaps(10, 10));

    // // Adding windows to the layout
    // layout.add_window("0x06c00003", tiling::window_options::master_stack(true, None));
    // layout.add_window("0x07c0000e", tiling::window_options::master_stack(false, Some(1 as f32)));
    // // layout.add_window("0x06a0000e", tiling::window_options::master_stack(false, Some(1 as f32)));

    // // Tiling
    // layout.retile();
}