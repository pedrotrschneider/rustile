pub mod master_stack_layout;

use self::master_stack_layout::{MasterStackLayout, MasterStackWindowOptions};

pub enum Layout {
    MasterStack(MasterStackLayout),
    None,
}

impl Layout {
    pub fn new_master_stack(desktop_id: i16) -> Self {
        return Self::MasterStack(MasterStackLayout::new(desktop_id));
    }
}

pub enum WindowOptions {
    MasterStack(MasterStackWindowOptions),
    None,
}

impl WindowOptions {
    pub fn new_master_stack(master: bool, stack_proportion: Option<f32>) -> Self {
        return Self::MasterStack(MasterStackWindowOptions { master, stack_proportion })
    }
}

pub enum LayoutOption {
    MasterPercentage(f32),
    OuterGaps((u16, u16, u16, u16)),
    InnerGaps((u16, u16)),
    None,
}

impl LayoutOption {
    pub fn new_master_percentage(percentage: f32) -> Self {
        return Self::MasterPercentage(percentage);
    }

    pub fn new_outer_gaps(gaps: (u16, u16, u16, u16)) -> Self {
        return Self::OuterGaps(gaps);
    }

    pub fn new_inner_gaps(gaps: (u16, u16)) -> Self {
        return Self::InnerGaps(gaps);
    }
}