use crate::wmctrlrs::{self, transform::Transform, desktop::Desktop, state::{Operation, Property}, WindowInfo};
use wmctrlrs::window::Window;

struct StackWindowID {
    window_id: String,
    proportion: f32,
}

pub struct MasterStackWindowOptions {
    pub master: bool,
    pub stack_proportion: Option<f32>,
}

impl MasterStackWindowOptions {
    pub fn new(master: bool, stack_proportion: Option<f32>) -> Self {
        return MasterStackWindowOptions { master, stack_proportion };
    }
}

pub struct MasterStackLayout {
    master_id: String,
    stack_ids: Vec<StackWindowID>,
    desktop_id: i16,
    master_percentage: f32,
    outer_gaps: (u16, u16, u16, u16), // left, right, top, bottom
    inner_gaps: (u16, u16), // horizontal, vertical
}

impl MasterStackLayout {
    pub fn new(desktop_id: i16) -> Self {
        return Self {
            master_id: "".to_owned(),
            stack_ids: vec!(), 
            desktop_id,
            master_percentage: 0.5,
            outer_gaps: (10, 10, 10, 10), // left, right, top, bottom
            inner_gaps: (10, 10), // horizontal, vertical
        };
    }

    pub fn set_master_percentage(&mut self, percentage: f32) {
        self.master_percentage = percentage;
    }

    pub fn set_outer_gaps(&mut self, gaps: (u16, u16, u16, u16)) {
        self.outer_gaps = gaps;
    }

    pub fn set_inner_gaps(&mut self, gaps: (u16, u16)) {
        self.inner_gaps = gaps;
    }

    pub fn get_window_info_if_valid(&self, window_id: &str) -> Option<Vec<WindowInfo>> {
        // let window_option = wmctrlrs::get_window_by_id(window_id);
        let window_info = wmctrlrs::get_window_info_batch(window_id)
            .desktop_id()
            .decorations()
            .collect();
        match window_info {
            Some(w) => match w[0] {
                WindowInfo::DesktopID(id) => if id != self.desktop_id { return None; },
                _ => return None,
            },
            None => return None,
        };
        
        return window_info;
    }

    pub fn is_window_valid(&self, window_id: &str) -> bool {
        return true;
    }

    pub fn add_window(&mut self, window_id: &str, window_options: MasterStackWindowOptions) {
        if window_options.master { // Send the window to the master
            if !self.master_id.is_empty() { // Check if there was a previous window on master
                // If there was, send it to the top of the stack
                self.stack_ids.push(
                    StackWindowID {
                        window_id: self.master_id.clone(),
                        proportion: 1 as f32 
                    }
                );
            }

            // Update the master window id
            self.master_id = window_id.to_owned();
        } else { // Send the window to the bottom of the stack
            self.stack_ids.insert(
                0, 
                StackWindowID {
                    window_id: window_id.to_owned(),
                    proportion: window_options.stack_proportion.unwrap(),
                }
            );
        }
    }

    pub fn retile(&mut self) {
        // Get option for the current master window
        let master_window_info = self.get_window_info_if_valid(&self.master_id);
        let master_decorations;

        match master_window_info { // Check if current master window is valid
            // If it is valid, get its data
            Some(window_infos) => match window_infos[1] {
                WindowInfo::Decorations(decorations) => master_decorations = decorations,
                _ => panic!("Error on master stack layout master window get infos")
            },
            // If it isn't, make the window at the top of the stack the new master and retile
            None => {
                if self.stack_ids.is_empty() { return }
                self.master_id = self.stack_ids.pop().unwrap().window_id;
                self.retile();
                return;
            },
        }

        // Get the data for the desktop of this layout.
        let desktop = Desktop::from_id(self.desktop_id);
        let desktop_working_area_width = desktop.get_working_area_width() - (self.outer_gaps.0 + self.outer_gaps.1);
        let desktop_working_area_height = desktop.get_working_area_height() - (self.outer_gaps.2 + self.outer_gaps.3);

        // Calculate master window width
        let master_percentage = if self.stack_ids.is_empty() { 1 as f32 } else { self.master_percentage };
        let mut master_width = desktop_working_area_width;
        master_width = ((master_width as f32) * master_percentage) as u16;
        master_width -= master_decorations.get_horizontal();
        if !self.stack_ids.is_empty() {
            master_width -= self.inner_gaps.0 / 2;
        }

        // Calculate master window height
        let master_height = desktop_working_area_height - master_decorations.get_horizontal();

        // Construct the new transform for the master window
        let master_transform = Transform::new(
            desktop.get_working_area_x() + self.outer_gaps.0 as i16,
            desktop.get_working_area_y() + self.outer_gaps.2 as i16,
            master_width,
            master_height,
        );

        // Remove all size constraining properties from the master window
        master_window.change_state(Operation::Remove, Property::Fullscreen);
        master_window.change_state(Operation::Remove, Property::MaximizedHorz);
        master_window.change_state(Operation::Remove, Property::MaximizedVert);

        // Transform the master window
        master_window.transform(&master_transform, 0);

        // End execution if stack is empty
        if self.stack_ids.is_empty() { return; }
        
        // Check if windows on the stack are still valid
        let mut stack_windows_options = vec!();
        for stack_id in self.stack_ids.iter().rev() {
            stack_windows_options.push(self.get_window_info_if_valid(&stack_id.window_id));
        }

        // Get only the valid windows, and remove invalid window ids form the stack
        let mut total_proportion = 0 as f32;
        let mut stack_windows_options_iter = stack_windows_options.iter();
        let mut stack_windows = vec!();
        self.stack_ids.retain(|element| {
            match stack_windows_options_iter.next().unwrap() {
                Some(w) => {
                    stack_windows.push((w.clone(), element.proportion));
                    total_proportion += element.proportion;
                    true
                },
                None => false,
            }
        });
        let num_windows = stack_windows.len();

        // Calculate the appropriate transform for each stack window
        let mut current_x = desktop.get_working_area_x() + master_window.get_total_width() as i16;
        current_x += (self.outer_gaps.0 + self.inner_gaps.0) as i16;
        let mut current_y = desktop.get_working_area_y() + self.outer_gaps.2 as i16;
        for (i, (stack_window, proportion)) in stack_windows.iter_mut().rev().enumerate() {
            // Calculate width of the stack window
            let mut window_width = desktop_working_area_width;
            window_width = (window_width as f32 * (1 as f32 - self.master_percentage)) as u16;
            window_width -= stack_window.get_decorations_horizontal();
            window_width -= self.inner_gaps.0 / 2;

            // Calculate height of the stack window
            let proportional_size = *proportion / total_proportion;
            let mut window_height = (desktop_working_area_height as f32 * proportional_size) as u16;
            window_height -= stack_window.get_decorations_vertical();

            if i == 0 { // First window on stack
                window_height -= self.inner_gaps.1 / 2;
            }
            else if i == num_windows - 1 { // Last window on stack
                window_height -= self.inner_gaps.1 / 2;
            } 
            else { // Window in the middle of the stack
                window_height -= self.inner_gaps.1;
            }

            // Construct the transform for the stack window
            let stack_transform = Transform::new(
                current_x,
                current_y,
                window_width,
                window_height,
            );

            // Remove all size constraining properties from the stack window
            stack_window.change_state(Operation::Remove, Property::Fullscreen);
            stack_window.change_state(Operation::Remove, Property::MaximizedHorz);
            stack_window.change_state(Operation::Remove, Property::MaximizedVert);
            
            // Transform the stack window
            stack_window.transform(&stack_transform, 1);
            
            // Update the current y position
            current_y += (stack_window.get_total_height() + self.inner_gaps.1) as i16;
        }
    }
}