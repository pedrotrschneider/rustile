pub mod utils;
pub mod state;
pub mod transform;
pub mod decorations;
pub mod desktop;
pub mod window;

use window::Window;
use transform::Transform;
use decorations::Decorations;

pub fn get_windows() -> Vec<Window> {
    let wmctrl_list_output = utils::run_wmctrl("-lGx");

    let mut windows = vec![];
    for window_info in wmctrl_list_output.lines() {
        if window_info.is_empty() { continue }
        windows.push(Window::from_info(window_info, true));
    }

    return windows;
}

// pub fn get_window_by_id(window_id: &str) -> Option<Window> {
//     let wmctrl_list_output = utils::run_wmctrl("-lGx");
    
//     for window_info in wmctrl_list_output.lines() {
//         if window_info.is_empty() { continue }
//         if window_info.contains(window_id) {
//             return Some(Window::from_info(window_info, true));
//         }
//     }
//     return None;
// }

#[derive(Debug)]
pub enum WindowInfo {
    DecimalID(i64),
    DesktopID(i16),
    WindowManagerClass(String),
    ClientName(String),
    Title(String),
    Geometry(Transform),
    Decorations(Decorations),
}

pub struct WindowInfoBuilder<'a> {
    window_id: &'a str,
    infos: Vec<WindowInfo>,
}

impl<'a> WindowInfoBuilder<'a> {
    pub fn decimal_id(mut self) -> Self{
        self.infos.push(WindowInfo::DecimalID(0));
        return self;
    }

    pub fn desktop_id(mut self) -> Self{
        self.infos.push(WindowInfo::DesktopID(0));
        return self;
    }

    pub fn window_manager_class(mut self) -> Self {
        self.infos.push(WindowInfo::WindowManagerClass("".to_owned()));
        return self;
    }

    pub fn client_name(mut self) -> Self {
        self.infos.push(WindowInfo::ClientName("".to_owned()));
        return self;
    }

    pub fn title(mut self) -> Self {
        self.infos.push(WindowInfo::Title("".to_owned()));
        return self;
    }

    pub fn geometry(mut self) -> Self {
        self.infos.push(WindowInfo::Geometry(Transform::empty()));
        return self;
    }

    pub fn decorations(mut self) -> Self {
        self.infos.push(WindowInfo::Decorations(Decorations::empty()));
        return self;
    }

    pub fn collect(&self) -> Option<Vec<WindowInfo>> {
        let wmctrl_info_string = utils::run_wmctrl("-lGx")
            .lines()
            .find(|x| x.starts_with(self.window_id))
            .map(|x| x.to_owned());
    
        let wmctrl_info;
        match wmctrl_info_string {
            Some(string) => {
                wmctrl_info = string
                    .split(" ")
                    .filter(|&x| !x.is_empty())
                    .map(|x| x.to_owned())
                    .collect::<Vec<String>>();
            },
            None => return None,
        }

        let mut output_infos = vec!();
        for info in &self.infos {
            output_infos.push(extract_info(self.window_id, &wmctrl_info, info));
        }

        return Some(output_infos);
    }
}


pub fn get_window_info(window_id: &str, info: WindowInfo) -> Option<WindowInfo> {
    let wmctrl_info_string = utils::run_wmctrl("-lGx")
        .lines()
        .find(|x| x.starts_with(window_id))
        .map(|x| x.to_owned());
    
    let wmctrl_info;
    match wmctrl_info_string {
        Some(string) => {
            wmctrl_info = string
                .split(" ")
                .filter(|&x| !x.is_empty())
                .map(|x| x.to_owned())
                .collect::<Vec<String>>();
        },
        None => return None,
    }
    
    return Some(extract_info(window_id, &wmctrl_info, &info));
}

pub fn get_window_info_batch(window_id: &str) -> WindowInfoBuilder {
    return WindowInfoBuilder { window_id , infos: vec!() };
}

fn extract_info(window_id: &str, wmctrl_info: &Vec<String>, info: &WindowInfo) -> WindowInfo {
    match info {
        // Get the window's decimal ID
        WindowInfo::DecimalID(_) => WindowInfo::DecimalID(
            i64::from_str_radix(
                window_id.strip_prefix("0x").unwrap(), 16
            ).unwrap()
        ),

        // Get the window's desktop ID
        WindowInfo::DesktopID(_) => WindowInfo::DesktopID(
            wmctrl_info[1]
                .to_string()
                .parse::<i16>()
                .unwrap()
        ),

        // Get the window's window manager class
        WindowInfo::WindowManagerClass(_) => WindowInfo::WindowManagerClass(
            wmctrl_info[6].to_string()
        ),

        // Get the window's client name
        WindowInfo::ClientName(_) => WindowInfo::ClientName(
            wmctrl_info[7].to_string()
        ),

        // Get the window's title
        WindowInfo::Title(_) => WindowInfo::Title(
            wmctrl_info[8..].join(" ").to_string()
        ),

        // Get the window's transform
        WindowInfo::Geometry(_) => WindowInfo::Geometry(Transform::new(
            wmctrl_info[2].parse::<i16>().unwrap(),
            wmctrl_info[3].parse::<i16>().unwrap(),
            wmctrl_info[4].parse::<u16>().unwrap(),
            wmctrl_info[5].parse::<u16>().unwrap(),
        )),

        // Get the window's decorations
        WindowInfo::Decorations(_) => WindowInfo::Decorations({
            let window_decimal_id = i64::from_str_radix(
                &window_id.strip_prefix("0x").unwrap(), 16
            ).unwrap();
            let decorations = utils::run_xprop(
                    &format!("_NET_FRAME_EXTENTS -id {}", window_decimal_id)
                ).strip_prefix("_NET_FRAME_EXTENTS(CARDINAL) = ")
                .unwrap()
                .lines().
                filter(|&x| !x.is_empty())
                .collect::<String>()
                .split(", ")
                .map(|x| x.to_owned().parse::<u16>().unwrap())
                .collect::<Vec<u16>>();
            
            Decorations::new(
                decorations[0],
                decorations[1], 
                decorations[2], 
                decorations[3]
            )
        }),
    }
}