use std::process::Command;

pub fn run_wmctrl(args: &str) -> String {
    return String::from_utf8(Command::new("sh")
        .arg("-c")
        .arg(format!("wmctrl {}", args))
        .output()
        .expect(&format!("Something went wrong running command wmctrl {}", args))
        .stdout).unwrap();
}

pub fn run_xprop(args: &str) -> String {
    return String::from_utf8(Command::new("sh")
        .arg("-c")
        .arg(format!("xprop {}", args))
        .output()
        .expect(&format!("Something went wrong running command wmctrl {}", args))
        .stdout).unwrap();
}