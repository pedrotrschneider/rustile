#[derive(Debug, Clone)]
pub struct Decorations {
    pub left: u16,
    pub right: u16,
    pub top: u16,
    pub bottom: u16,
}

impl Decorations {
    pub fn empty() -> Self {
        return Self {
            left: 0,
            right: 0,
            top: 0,
            bottom: 0,
        };
    }

    pub fn new(left: u16, right: u16, top: u16, bottom: u16) -> Self {
        return Self { left, right, top, bottom };
    }

    pub fn get_horizontal(&self) -> u16 {
        return self.left + self.right;
    }

    pub fn get_vertical(&self) -> u16 {
        return self.top + self.bottom;
    }

    pub fn get_left(&self) -> u16 {
        return self.left;
    }

    pub fn get_right(&self) -> u16 {
        return self.right;
    }

    pub fn get_top(&self) -> u16 {
        return self.top;
    }

    pub fn get_bottom(&self) -> u16 {
        return self.bottom;
    }
}