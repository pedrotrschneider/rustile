pub enum Operation {
    Add,
    Remove,
    Toggle,
}

impl Operation {
    pub fn get(&self) -> String {
        match *self {
            Operation::Add => "add".to_owned(),
            Operation::Remove => "remove".to_owned(),
            Operation::Toggle => "toggle".to_owned(),
        }
    }
}

pub enum Property {
    Modal,
    Sticky,
    MaximizedVert,
    MaximizedHorz,
    Shaded,
    SkipTaskbar,
    SkipPager,
    Hidden,
    Fullscreen,
    Above,
    Below,
}

impl Property {
    pub fn get(&self) -> String {
        match *self {
            Property::Modal => "modal".to_owned(),
            Property::Sticky => "sticky".to_owned(),
            Property::MaximizedVert => "maximized_vert".to_owned(),
            Property::MaximizedHorz => "maximized_horz".to_owned(),
            Property::Shaded => "shaded".to_owned(),
            Property::SkipTaskbar => "skip_taskbar".to_owned(),
            Property::SkipPager => "skip_pager".to_owned(),
            Property::Hidden => "hidden".to_owned(),
            Property::Fullscreen => "fullscreen".to_owned(),
            Property::Above => "above".to_owned(),
            Property::Below => "below".to_owned(),
        }
    }
}