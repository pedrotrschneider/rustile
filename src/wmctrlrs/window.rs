use super::utils;
use super::state::{Operation, Property};
use super::transform::Transform;
use super::decorations::Decorations;

use std::i64;

#[derive(Debug, Clone)]
pub struct Window {
    hex_id: String,
    decimal_id: i64,
    desktop_id: i16,
    window_manager_class: String,
    client_name: String,
    title: String,
    transform: Transform,
    decorations: Decorations
}

impl Window {
    pub(crate) fn new(hex_id: String, decimal_id: i64, desktop_id: i16, window_manager_class: String, client_name: String, title: String, transform: Transform, decorations: Decorations) -> Self {
        return Window { hex_id, decimal_id, desktop_id, window_manager_class, client_name, title, transform, decorations };
    }

    pub(crate) fn from_info(window_info: &str, calculate_decorations: bool) -> Window {
        let window_info_split: Vec<&str> = window_info
            .split(" ")
            .filter(|&x| !x.is_empty())
            .collect();
        
        let window_id = window_info_split[0].to_string();
        let window_decimal_id = i64::from_str_radix(&window_id.strip_prefix("0x").unwrap(), 16).unwrap();
        let window_desktop_id = window_info_split[1].to_string().parse::<i16>().unwrap();
        let window_transform = Transform {
            x: window_info_split[2].parse::<i16>().unwrap(),
            y: window_info_split[3].parse::<i16>().unwrap(),
            width: window_info_split[4].parse::<u16>().unwrap(),
            height: window_info_split[5].parse::<u16>().unwrap(),
        };
        let window_window_manager_class = window_info_split[2].to_string();
        let window_client_name = window_info_split[3].to_string();
        let mut window_title = window_info_split[4].to_string();
        for i in 5..window_info_split.len() {
            window_title += " ";
            window_title += window_info_split[i];
        }

        let mut window_decorations = Decorations::empty();

        if calculate_decorations {
            let xprop_args = format!("_NET_FRAME_EXTENTS -id {}", window_decimal_id); 
            let xprop_frame_extents = utils::run_xprop(&xprop_args)
                .strip_prefix("_NET_FRAME_EXTENTS(CARDINAL) = ")
                .unwrap()
                .lines().
                filter(|&x| !x.is_empty())
                .collect::<String>()
                .split(", ")
                .map(|x| x.to_owned())
                .collect::<Vec<String>>();
            
            window_decorations = Decorations {
                left: xprop_frame_extents[0].parse::<u16>().unwrap(),
                right: xprop_frame_extents[1].parse::<u16>().unwrap(),
                top: xprop_frame_extents[2].parse::<u16>().unwrap(),
                bottom: xprop_frame_extents[3].parse::<u16>().unwrap(),
            };
        }

        return Window::new(window_id, window_decimal_id, window_desktop_id, window_window_manager_class, window_client_name, window_title, window_transform, window_decorations);
    }
    
    pub fn get_hex_id(&self) -> &str {
        return self.hex_id.as_str();
    }

    pub fn get_decimal_id(&self) -> i64 {
        return self.decimal_id;
    }

    pub fn get_desktop_id(&self) -> i16 {
        return self.desktop_id;
    }

    pub fn get_window_manager_class(&self) -> &str {
        return self.window_manager_class.as_str();
    } 

    pub fn get_client_name(&self) -> &str {
        return self.client_name.as_str();
    }

    pub fn get_title(&self) -> &str {
        return self.title.as_str();
    }

    pub fn get_x(&self) -> i16 {
        return self.transform.x;
    }

    pub fn get_y(&self) -> i16 {
        return self.transform.y;
    }

    pub fn get_width(&self) -> u16 {
        return self.transform.width;
    }

    pub fn get_height(&self) -> u16 {
        return self.transform.height;
    }

    pub fn get_decorations_left(&self) -> u16 {
        return self.decorations.left;
    }

    pub fn get_decorations_right(&self) -> u16 {
        return self.decorations.right;
    }

    pub fn get_decorations_horizontal(&self) -> u16 {
        return self.decorations.left + self.decorations.right;
    }

    pub fn get_decorations_top(&self) -> u16 {
        return self.decorations.top;
    }

    pub fn get_decorations_bottom(&self) -> u16 {
        return self.decorations.bottom;
    }

    pub fn get_decorations_vertical(&self) -> u16 {
        return self.decorations.top + self.decorations.bottom;
    }

    pub fn get_total_width(&self) -> u16 {
        return self.decorations.left + self.transform.width + self.decorations.right;
    }

    pub fn get_total_height(&self) -> u16 {
        return self.decorations.top + self.transform.height + self.decorations.bottom;
    }

    pub fn refresh(&mut self) {
        let wmctrl_list_output = utils::run_wmctrl("-lGx");
        for window_info in wmctrl_list_output.lines() {
            if window_info.is_empty() { continue }
            if window_info.contains(&self.hex_id) {
                let new_window = Window::from_info(window_info, false);
                self.desktop_id = new_window.desktop_id;
                self.window_manager_class = new_window.window_manager_class;
                self.client_name = new_window.client_name;
                self.title = new_window.title;
                self.transform = new_window.transform;
            }
        }
    }

    pub fn set_title(&mut self, title: &str) {
        let args = format!("-r {} -N {}", self.hex_id, title);
        utils::run_wmctrl(&args);
        self.refresh();
    }

    pub fn set_icon_title(&mut self, title: &str) {
        let args = format!("-r {} -I {}", self.hex_id, title);
        utils::run_wmctrl(&args);
        self.refresh();
    }

    pub fn set_both_title(&mut self, title: &str) {
        let args = format!("-r {} -T {}", self.hex_id, title);
        utils::run_wmctrl(&args);
        self.refresh();
    }

    pub fn change_state(&mut self, operation: Operation, property: Property) {
        let args = format!(
            "-r {} -i -b {},{}", 
            self.hex_id,
            operation.get(), 
            property.get()
        );
        utils::run_wmctrl(&args);
        self.refresh();
    }

    pub fn transform(&mut self, transform: &Transform, gravity: u16) {
        let args = format!("-r {} -i -e {},{}", self.hex_id, gravity, transform.get());
        utils::run_wmctrl(&args);
        self.refresh();
    }

    pub fn move_to_desktop(&mut self, desktop_id: i16) {
        let args = format!("-r {} -i -t {}", self.hex_id, desktop_id);
        utils::run_wmctrl(&args);
        self.refresh();
    }

    pub fn activate(&mut self) {
        let args = format!("-R {} -i", self.hex_id);
        utils::run_wmctrl(&args);
        self.refresh();
    }

    pub fn raise(&self) {
        let args = format!("-a {} -i", self.hex_id);
        utils::run_wmctrl(&args);
    }

    pub fn close(&self) {
        let args = format!("-c {} -i", self.hex_id);
        utils::run_wmctrl(&args);
    }
}