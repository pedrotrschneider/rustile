use super::utils;
use super::transform::Transform;

pub fn list_desktops() -> String {
    return utils::run_wmctrl("-d");
}

pub fn switch_to_desktop(desktop_id: i16) {
    utils::run_wmctrl(&format!("-s {}", desktop_id));
}

pub fn get_current_desktop() -> i16 {
    return utils::run_wmctrl("-d")
        .lines()
        .find(|line| line.contains("*"))
        .unwrap()
        .split(" ")
        .filter(|&x| !x.is_empty())
        .collect::<Vec<&str>>()[0]
        .parse::<i16>()
        .unwrap();
}

pub struct Desktop {
    id: i16,
    working_area: Transform,
}

impl Desktop {
    pub fn current() -> Self {
        let current_desktop_info = utils::run_wmctrl("-d")
            .lines()
            .find(|line| line.contains("*"))
            .unwrap()
            .split(" ")
            .filter(|&x| !x.is_empty())
            .map(|x| x.to_owned())
            .collect::<Vec<String>>();

        let desktop_id = current_desktop_info[0]
            .parse::<i16>()
            .unwrap();
        
        let working_area_pos = current_desktop_info[7]
            .split(",")
            .map(|x| x.to_owned())
            .map(|x| x.parse::<i16>().unwrap())
            .collect::<Vec<i16>>();
        
        let working_area_size = current_desktop_info[8]
            .split("x")
            .map(|x| x.to_owned())
            .map(|x| x.parse::<u16>().unwrap())
            .collect::<Vec<u16>>();
        
        return Self {
            id: desktop_id,
            working_area: Transform::new(
                working_area_pos[0],
                working_area_pos[1],
                working_area_size[0],
                working_area_size[1]
            ),
        };
    }

    pub fn from_id(desktop_id: i16) -> Self {
        let desktop_info = utils::run_wmctrl("-d")
            .lines()
            .find(|line| line.starts_with(&desktop_id.to_string()))
            .unwrap()
            .split(" ")
            .filter(|&x| !x.is_empty())
            .map(|x| x.to_owned())
            .collect::<Vec<String>>();

        let desktop_id = desktop_info[0]
            .parse::<i16>()
            .unwrap();
        
        let working_area_pos = desktop_info[7]
            .split(",")
            .map(|x| x.to_owned())
            .map(|x| x.parse::<i16>().unwrap())
            .collect::<Vec<i16>>();
        
        let working_area_size = desktop_info[8]
            .split("x")
            .map(|x| x.to_owned())
            .map(|x| x.parse::<u16>().unwrap())
            .collect::<Vec<u16>>();
        
        return Self {
            id: desktop_id,
            working_area: Transform::new(
                working_area_pos[0],
                working_area_pos[1],
                working_area_size[0],
                working_area_size[1]
            ),
        };
    }

    pub fn get_id(&self) -> i16 {
        return self.id;
    }

    pub fn get_working_area_x(&self) -> i16 {
        return self.working_area.x;
    }

    pub fn get_working_area_y(&self) -> i16 {
        return self.working_area.y;
    }

    pub fn get_working_area_width(&self) -> u16 {
        return self.working_area.width;
    }

    pub fn get_working_area_height(&self) -> u16 {
        return self.working_area.height;
    }
}