#[derive(Debug, Clone)]
pub struct Transform {
    pub x: i16,
    pub y: i16,
    pub width: u16,
    pub height: u16,
}

impl Transform {
    pub fn new(x: i16, y: i16, width: u16, height: u16) -> Self {
        return Transform { x, y, width, height };
    }

    pub fn from_other(transform: &Transform) -> Self {
        return Transform { 
            x: transform.x, 
            y: transform.y, 
            width: transform.width, 
            height: transform.height 
        };
    }

    pub fn empty() -> Self {
        return Transform { x: 0, y: 0, width: 0, height: 0 };
    }

    pub fn get(&self) -> String {
        return format!("{},{},{},{}", self.x, self.y, self.width, self.height);
    }
}