pub mod layouts;

use layouts::{Layout, WindowOptions, LayoutOption};
use layouts::master_stack_layout::MasterStackLayout;

impl Layout {
    pub fn set_layout_option(&mut self, option: LayoutOption) {
        match self {
            Layout::MasterStack(l) => set_master_stack_layout_option(l, option),
            _ => (),
        }
    }

    pub fn add_window(&mut self, window_id: &str, window_options: WindowOptions) {
        match self {
            Layout::MasterStack(l) => add_window_master_stack(l, window_id, window_options),
            _ => (),
        }
    }
    
    pub fn retile(&mut self) {
        match self {
            Layout::MasterStack(l) => l.retile(),
            _ => (),
        }
    }
}

pub mod layout {
    pub fn master_stack(desktop_id: i16) -> super::Layout {
        return super::Layout::new_master_stack(desktop_id);
    }
}

pub mod window_options {
    pub fn master_stack(master: bool, stack_proportion: Option<f32>) -> super::WindowOptions {
        return super::WindowOptions::new_master_stack(master, stack_proportion);
    }
}

pub mod layout_option {
    pub fn master_percentage(percentage: f32) -> super::LayoutOption {
        return super::LayoutOption::new_master_percentage(percentage);
    }

    pub fn outer_gaps(left: u16, right: u16, top: u16, bottom: u16) -> super::LayoutOption {
        return super::LayoutOption::new_outer_gaps((left, right, top, bottom));
    }

    pub fn inner_gaps(horizontal: u16, vertical: u16) -> super::LayoutOption {
        return super::LayoutOption::new_inner_gaps((horizontal, vertical));
    }
}

fn set_master_stack_layout_option(layout: &mut MasterStackLayout, option: LayoutOption) {
    match option {
        LayoutOption::MasterPercentage(p) => layout.set_master_percentage(p),
        LayoutOption::OuterGaps(gaps) => layout.set_outer_gaps(gaps),
        LayoutOption::InnerGaps(gaps) => layout.set_inner_gaps(gaps),
        _ => (),
    }
}

fn add_window_master_stack(layout: &mut MasterStackLayout, window_id: &str, window_options: WindowOptions) {
    match window_options {
        WindowOptions::MasterStack(wo) => layout.add_window(window_id, wo),
        _ => panic!("Window options type doesn't match Master Stack Layout"),
    }
}