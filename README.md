# Rustile

`Rustile` is program written in `Rust` that does window-manager-agnostic automatic window tiling.

This means that `Rustile` should work on Gnome, KDE, XFCE, or any other window manager / desktop environment under the sun (any window manager that is EWMH/NetWM compliant, of course).

Unfortunately, it uses `wmctrl` for window manipulation, which means it only works on X display servers. Since there is no alternative for it on Wayland, there is no way to make it work in there (and trust me, I tried).

## Why?

Because I like automatic window tiling, but don't have the time or patience to deal with configuring and maintaining a tiling window manager. I would much rather use the likes of Gnome or KDE, and have tiling accomplished through an external program such as `Rustile`.

KDE has Bismuth, and it works pretty well, but I usually prefer Gnome instead of KDE. Unfortunately, there is nothing even similar to Bismuth on Gnome, especially Gnome 43. Closest I could find was PaperWM, and It's really good, but not for me.

That's why I decided to write `Rustile`.

## Dependencies

As I said, it depends on `wmctrl` for window manipulation. Since I gave up on Wayland support, I decided to fully embrace de suite of applications for X, so it also depends on `xprop` for now, but I'll probably need other programs like `xdotool` and `xwininfo` in the future. But they should all come pre-installed on any linux distro with an X window manager.

Also, `Rust`, obviously.

## Progress

`Rustile` is still very early software and not nearly usable yet. It does tile windows if you ask it nicely, but the usability is awful. This is what I plan on implementing next:

- [] Configuration file for customization
- [] Rustile Daemon for persistence
- [] Other tiling layouts